import {NS} from "@ns";
import {home, qAssert, qGlob, qMain, qStorage} from "s/lib/my";

export async function sync(ns: NS, host: string) {
    qAssert(host != home, `Trying to sync home`);
    var rgx = "^s/";
    var files = qGlob(ns, home, rgx);
    ns.scp(files, host, home);
}

var stor = new qStorage.Group("visit");

function visitor(ns: NS, path: string[]) {
    var cur = ns.getHostname();
    for (var host in ns.scan()) {
        if (!stor.has(host)) {
            stor.set(host, true);
            qAssert(ns.singularity.connect(host));
            ns.tprint(`visiting ${ns.getHostname()} ${host} by ${path}`);
            visitor(ns, [...path, host]);
            qAssert(ns.singularity.connect(cur));
        }
    }
}

export async function main(ns: NS) {
    var m = new qMain(ns, {len: 0});
    if (ns.getHostname() == home) {
        stor.clear();
    }
    visitor(ns, []);
}
