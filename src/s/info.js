function strseconds(SECONDS) {
    return new Date(SECONDS * 1000).toISOString(); // .substring(11, 16);
}
/** @param {dict} data @return str */
function strdict(data, sep = "\n") {
    var s = "";
    for (var k in data) {
        s += (s.length == 0 ? "" : sep) + k + "=" + data[k];
    }
    return s;
}
/** @param {NS} ns @param {str} server @return str */
function getInfo(ns, server) {
    var data = {
        "growtime": strseconds(ns.getGrowTime(server)),
        "hacktime": strseconds(ns.getHackTime(server)),
    };
    var s = strdict(data);
    var f = "info-" + server + ".txt";
    // ns.write(f, s);
    // ns.scp(f, "home");
    return s;
}
/** @param {NS} ns */
export async function main(ns) {
    var servers = ns.scan();
    var s = "";
    for (var server of servers) {
        s += server + "\n" + getInfo(ns, server) + "\n";
    }
    ns.print(s);
}
