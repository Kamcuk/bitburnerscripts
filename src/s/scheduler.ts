import {NS} from "@ns";
import {home, qColumnize, qKillThisScriptExceptItself, qMain, qScanRecursive} from "s/lib/my";
import {qGainRoot} from "s/rooter";

var m: qMain;
var ns: NS;

// ------------------------------------------------------------------

type DoitOpts = {
    calcthreads?: number | boolean;
    threads?: number;
    maxthreads?: number;
    savemem?: number;
};

function doit(ns: NS, host: string, script: string, args?: string[], opts?: DoitOpts): number {
    var serverMaxRam = ns.getServerMaxRam(host) - (opts ? opts.savemem || 0 : 0);
    var serverUsedRam = ns.getServerUsedRam(host);
    var scriptRam = ns.getScriptRam(script, host);
    var threads: number = !opts ? 1 :
        opts.threads ? opts.threads :
            opts.calcthreads ? Math.floor((serverMaxRam - serverUsedRam) / scriptRam) :
                1;
    if (opts && opts.maxthreads) {
        threads = Math.min(threads, opts.maxthreads);
    }
    var msg = `exec host=${host} threads=${threads}=(${serverMaxRam}-${serverUsedRam})/${scriptRam}: ${script} ${args}`;
    if (scriptRam == 0) {
        m.error(`could not ${msg}: script missing`);
        return 0;
    }
    if (threads < 1) {
        m.error(`could not ${msg} out of memory`);
        return 0;
    }
    m.assert(threads * scriptRam <= serverMaxRam - serverUsedRam,
        `${threads} ${scriptRam} ${serverMaxRam} ${serverUsedRam}`
    );
    m.log(`${msg}`);
    var pid = ns.exec(script, host, {threads: threads}, ...(args || []), `hostname=${host}`, ...m.map2args());
    if (!pid) {
        m.error(`could not ${msg}`);
        return 0;
    }
    return pid;
}

async function doitwait(ns: NS, host: string, script: string, args?: string[], opts?: DoitOpts) {
    var pid = doit(ns, host, script, args, opts);
    if (!pid) return;
    while (ns.ps(host).find(p => p.pid == pid)) {
        await ns.sleep(500);
        m.debug(`waiting for host=${host} pid=${pid} ${script} ${args}`)
    }
}

// ------------------------------------------------------------------

const saveMemOnHome = 30;

class Action {
    constructor(
        public host: string,
        public action: string,
        public hack: string,
        public pid?: number,
        public threads?: number) {}
    str(): string {
        return `Action(${this.host},${this.action},${this.hack},${this.pid})`;
    }
    actionscript(): string {
        return `s/a/${this.action}.js`;
    }
    isable(): boolean {
        return ns.getServerMaxRam(this.host) - ns.getServerUsedRam(this.host) - (this.host == home ? saveMemOnHome : 0)
            >= ns.getScriptRam(this.actionscript(), this.host);
    }
    doit(): number {
        var ns = m.ns;
        m.assert(!this.pid);
        return this.pid = doit(ns, this.host, this.actionscript(), [this.hack], {
            maxthreads: this.threads,
            calcthreads: true,
            savemem: this.host == home ? saveMemOnHome : 0,
        });
    }
}

// ------------------------------------------------------------------

function get_next_periodic_action(): Action | undefined {
    return;
}

function host_money_cmp(a: string, b: string) {
    return ns.getServerMoneyAvailable(b) - ns.getServerMoneyAvailable(a);
}

function get_next_action(hosts: string[], actions: Action[]): Action | undefined {
    var ns = m.ns;
    var ret = get_next_periodic_action();
    if (ret) return ret;
    //
    var runner: string = hosts.
        filter(h => ns.getServerMaxRam(h) && ns.hasRootAccess(h)).
        map<[string, number]>(h =>
            [h, ns.getServerMaxRam(h) - ns.getServerUsedRam(h) - (h == home ? saveMemOnHome : 0)]).
        sort((a, b) => b[1] - a[1])[0][0];
    //
    var hostsWithMoney: string[] = hosts.
        filter(h => h != home &&
            ns.getHackingLevel() > ns.getServerRequiredHackingLevel(h) &&
            ns.getServerMaxMoney(h) > 0).
        sort(host_money_cmp);
    var sumOfActions: number = actions.reduce<number>((o, a) => o + (a.threads || 0), 0);
    //
    var HostAction_to_actions: Map<string, number> = new Map();
    for (var a of actions) {
        var idx = `${a.hack} ${a.action}`;
        HostAction_to_actions.set(idx, (HostAction_to_actions.get(idx) || 0) + (a.threads || 0));
    }
    //
    var possibleHostActions: string[] = [];
    for (var h of hostsWithMoney) {
        if (ns.getServerSecurityLevel(h) > 1 && ns.getServerSecurityLevel(h) > ns.getServerMinSecurityLevel(h) + 5) {
            possibleHostActions.push(`${h} weaken`);
        }
        if (ns.getServerMoneyAvailable(h) < ns.getServerMaxMoney(h) * 0.75 || ns.getServerMoneyAvailable(h) < 1) {
            possibleHostActions.push(`${h} grow`);
        }
        possibleHostActions.push(`${h} hack`);
    }
    function hostScore(host: string) {
        return ns.getServerMoneyAvailable(host) / ns.getServerMaxMoney(host);
    }
    var sumOfScores: number = possibleHostActions.
        map(h => h.split(" ")[0]).
        reduce<number>((o, h) => o + hostScore(h), 0);
    //
    var chosen: Action | undefined;
    for (var i of possibleHostActions) {
        var host = i.split(" ")[0];
        var action = i.split(" ")[1];
        var hostActionActions: number = HostAction_to_actions.get(i) || 0;
        var scoreNorm = hostScore(host) / sumOfScores;
        var actionsNorm = hostActionActions / sumOfActions;
        m.debug(`${scoreNorm.toFixed(4)} > ${actionsNorm.toFixed(4)} ${host} ${action}`);
        if (actions.length == 0 || scoreNorm > actionsNorm) {
            var maxthreads = 100;
            chosen = new Action(runner, action, host, 0, maxthreads);
            break;
        }
    }
    return chosen;
}

// ------------------------------------------------------------------

function* find_actions(hosts: string[]): Generator<Action> {
    for (var host of hosts) {
        for (var p of m.ns.ps(host)) {
            var action = p.filename.replace(new RegExp("^s/a/(.*).js$"), "$1");
            if (action != p.filename) {
                var arg = p.args.map(a => a.toString()).filter(a => !a.includes("="))[0];
                yield new Action(host, action, arg, p.pid, p.threads);
            }
        }
    }
    //ret.forEach(a => m.debug(`Running action: ${a.str()}`));
}

function runner_once(): boolean {
    var hosts = qScanRecursive(ns);
    var actions = Array.from(find_actions(hosts));
    var next = get_next_action(hosts, actions);
    if (next && next.isable()) {
        if (m.has("dryrun")) {
            m.log(`${next.str()}`);
        } else {
            next.doit();
            return true;
        }
    }
    return false;
}

async function runner_main() {
    qKillThisScriptExceptItself(ns);
    var hosts = qScanRecursive(ns);
    qGainRoot(m, hosts);
    await doitwait(ns, home, "s/sync.js");
    while (1) {
        runner_once();
        await ns.sleep(1000);
    }
}

function runner_ps() {
    var hosts = qScanRecursive(ns);
    var actions = find_actions(hosts);
    var legend = "host th file hack money grow".split(" ");
    var txt: any[][] = [];
    for (var a of actions) {
        m.assert(!!a.pid);
        var p = ns.getRunningScript(a.pid);
        m.assert(!!p);
        p = p!;
        var host = a.hack;
        txt.push([
            a.host, p.threads, p.filename, host,
            ns.getServerMoneyAvailable(host).toFixed(0) + "/" + ns.getServerMaxMoney(host).toFixed(0),
            ns.getGrowTime(host).toFixed(0)]);
    }
    txt.sort((a, b) => a.at(-1) == b.at(-1) ? a[0] > b[0] ? -1 : 1 : a.at(-1) > b.at(-1) ? -1 : 1);
    for (var line of qColumnize([legend, ...txt])) {
        m.log(`${line}`);
    }
}

export async function main(ns_: NS) {
    ns = ns_;
    m = new qMain(ns);
    ns.disableLog("ALL");
    if (m.args.length == 0) {
        await runner_main();
    } else if (m.args[0] == "once") {
        runner_once();
    } else if (m.args[0] == "ps") {
        runner_ps();
    } else if (m.args[0] == "stop") {
        qKillThisScriptExceptItself(ns);
        var hosts = qScanRecursive(ns);
        var actions = find_actions(hosts);
        for (var a of actions) {
            m.log(`killing ${a.pid}`);
            ns.kill(a.pid!);
        }
    } else {
        m.assert(false, "invalid arguments");
    }
}
