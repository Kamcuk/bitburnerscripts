import {NS} from "@ns";
import {qColumnize, qMain, qScanRecursive} from "s/lib/my";

var m: qMain;

function fl(txt: string, cond: boolean) {
    return cond ? txt : "-";
}

function numfmt(num: number | undefined): string {
    num = num || 0;
    if (num < 0.01)
        return "0";
    if (num < 10)
        return `${num.toFixed(2)}`;
    if (num < 100)
        return `${num.toFixed(1)}`;
    if (num < 1000)
        return `${num.toFixed(0)}`;
    if ((num /= 1000) < 10)
        return `${num.toFixed(1)}K`;
    if (num < 1000)
        return `${num.toFixed(0)}K`;
    if ((num /= 1000) < 10)
        return `${num.toFixed(1)}M`;
    if (num < 1000)
        return `${num.toFixed(0)}M`;
    if ((num /= 1000) < 10)
        return `${num.toFixed(1)}G`;
    if (num < 1000)
        return `${num.toFixed(0)}G`;
    if ((num /= 1000) < 10)
        return `${num.toFixed(1)}T`;
    if (num < 1000)
        return `${num.toFixed(0)}T`;
    return `${num}??`;
}

var global_legend: string[];

/** return score of the row */
function myScore(a: (number | string)[]): (number | string)[] {
    var ret: (number | string)[] = [];
    if (m.has("sort")) {
        var sort = m.get("sort");
        var col = /^[0-9]+$/.test(sort) ?
            +sort - 1 :
            global_legend.findIndex(l => l == sort);
        m.assert(col in a, `no such column as ${sort}`);
        ret.push(a[col]);
    }
    if (typeof (a[0]) != "string") m.throw();
    var host: string = a[0];
    var ns = m.ns;
    ret.push(
        ns.hasRootAccess(host) ? 1 : 0,
        ns.getServerUsedRam(host),
        ns.getServerMaxRam(host),
        host,
    );
    return ret;
};

/** compare scores of rows */
function myCmpScores(a: (number | string)[], b: (number | string)[]) {
    for (var i in a) {
        if (typeof (a[i]) == "string") {
            m.debug(`${a[i]} compare ${b[i]}`);
            // @ts-ignore
            return a[i].localeCompare(b[i]);
        } else {
            m.debug(`${a[i]} - ${b[i]}`);
            // @ts-ignore
            return b[i] - a[i];
        }
    }
}

/** Convert row to score and compare */
function myCmp(a: (number | string)[], b: (number | string)[]) {
    return myCmpScores(myScore(a), myScore(b));
}

export async function main(ns: NS) {
    m = new qMain(ns, {
        opts: [
            "sort:index of the column to sort on",
            "grep:",
        ]
    });
    // Get hosts.
    var hosts = qScanRecursive(ns);
    if (m.args.length) {
        hosts = hosts.filter(h => m.args.find(a => h.includes(a)));
    }
    // Get the array.
    var pretxt: any[] = [];
    for (var host of hosts) {
        var s = ns.getServer(host);
        pretxt.push({
            host: host,
            flags: ""
                + fl("R", s.hasAdminRights)
                + fl("H", ns.getHackingLevel() >= (s.requiredHackingSkill || 0))
                + fl("B", s.backdoorInstalled || false),
            ports: ""
                + fl("S", s.sshPortOpen)
                + fl("F", s.ftpPortOpen)
                + fl("H", s.httpPortOpen)
                + fl("M", s.smtpPortOpen)
                + fl("Q", s.sqlPortOpen)
                + `${s.numOpenPortsRequired || "?"}`, // `
            ram: `${s.ramUsed}/${s.maxRam}`,
            org: s.organizationName,
            diffi: s.baseDifficulty + "/" + s.minDifficulty,
            hackd: (s.hackDifficulty || 0).toFixed(0),
            reqH: s.requiredHackingSkill || "?",
            money: `${numfmt(s.moneyAvailable)}/${numfmt(s.moneyMax)}`,
            moneyAvailable: s.moneyAvailable,
            moneyMax: s.moneyMax,
        });
    }
    var legend: string[] = global_legend = Object.keys(pretxt[0]);
    var txt: (string | number)[][] = pretxt.map(e => Object.values(e));
    // Apply user requests settings.
    if (m.has("grep")) {
        var rgx = new RegExp(m.get("grep"));
        txt = txt.filter(l => rgx.test(l.map(e => e.toString()).join(" ")));
    }
    txt.sort(myCmp);
    if (m.has("rev")) {
        txt = txt.reverse();
    }
    {
        var cols = m.has("cols") ? +m.get("cols") : 9;
        legend = legend.splice(0, cols);
        txt = txt.map(l => l.splice(0, cols));
    }
    if (m.has("head")) {
        txt = txt.splice(0, +m.get("head"));
    }
    if (m.has("tail")) {
        txt = txt.splice(txt.length - +m.get("tail"));
    }
    // Output.
    if (pretxt.length > 1) {
        ns.tprint(`flags: Rooted Hackable Backdoored`);
        for (var [i, line] of qColumnize([legend, ...txt]).entries()) {
            ns.tprint(line);
        }
    }
    ns.tprint(`Found ${txt.length} hosts`);
}
