import {NS} from "@ns";
import {log, qMain} from "s/lib/my";
export async function main(ns: NS) {
    var m = new qMain(ns, {len: 1});
    var host = m.args[0];
    ns.run("s/ps.js", 1, host);
    log.warn(ns, `killall ${host}`);
    var count = ns.ps(host).length;
    if (!ns.killall(host)) {
        log.warn(ns, `did not kill nothing`);
    } else {
        log.log(ns, `killed ${count} processes`);
    }
}
