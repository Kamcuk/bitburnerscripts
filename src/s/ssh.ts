import {NS} from "@ns";
import {home, log, qMain} from "s/lib/my";
export async function main(ns: NS) {
    var m = new qMain(ns, {min:2});
    var host = m.args[0];
    var script = `s/${m.args[1]}.js`;
    var args = [];
    for (var arg in ns.args.slice(2)) {
        args.push(`${arg}`);
    }
    if (!ns.fileExists(script, home)) {
        log.error(ns, `file ${script} does not exists`);
    }
    if (!ns.scp(script, host, home)) {
        log.error(ns, `scp ${host} failed`);
    }
    if (!ns.exec(script, host, 1, ...args)) {
        log.error(ns, `exec ${host} failed`);
    }
}
