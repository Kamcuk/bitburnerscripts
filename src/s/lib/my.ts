import { NS, Server } from "@ns";
import { qMain } from "s/lib/qmain";
export { qAssert, qMain } from "s/lib/qmain";

export const home: string = "home";

/** calculate hash of a string */
export function qStrhash(txt: string): number {
    return txt.split('').reduce((prevHash, currVal) => (((prevHash << 5) - prevHash) + currVal.charCodeAt(0)) | 0, 0);
}

/** union two sets */
export function union<T>(...sets: Set<T>[]): Set<T> {
    return sets.reduce((combined, list) => {
        return new Set([...combined, ...list]);
    }, new Set());

}

/** Convert map to string */
export function strmap<N, M>(map: Map<N, M>, sep: string = "\n"): string {
    let str = '';
    for (const [key, value] of map) {
        str += `${key}=${value}${sep}`;
    }
    return str;
}

export function sum(arr: number[], init: number = 0): number {
    return arr.reduce((a, b) => a + b, init);
}


class qScanHost {
    down: qScanHost[] = [];
    constructor(public host: string, public up?: qScanHost) {
        if (up) up.down.push(this);
    }
    find(host: string) {
        return this.host == host ? this : this.down.find(h => h.host == host);
    }
    len(): number {
        return this.parents().length;
    }
    parents(): qScanHost[] {
        var ret: qScanHost[] = [];
        var up: qScanHost | undefined = this.up;
        while (up) {
            ret.push(up);
            up = up.up;
        }
        return ret;
    }
    array(): string[] {
        var ret: string[] = [];
        for (var s of this.down) {
            ret.push(s.host);
            ret.push(...s.array());
        }
        return ret;
    }
};


export function qScanRecursiveTree(
    ns: NS,
    root?: qScanHost,
    up?: qScanHost,
): qScanHost {
    root = root || new qScanHost(ns.getHostname());
    up = up || root;
    for (var h of ns.scan(up.host)) {
        var exists = root.find(h);
        if (!exists) {
            qScanRecursiveTree(ns, root, new qScanHost(h, up));
        }
    }
    return root;
}

/**
 * Scan hosts recursively and return a list with jumphosts needed to jump to that host
 * @return A map with host->list of jump hosts.
 **/
export function qScanRecursiveMap(
    ns: NS,
    host: string = ns.getHostname(),
    way: string[] = [],
    scanned: Map<string, string[]> = new Map(),
): Map<string, string[]> {
    scanned.set(host, way);
    way.push(host);
    for (var h of ns.scan(host)) {
        if (!scanned.has(h)) {
            qScanRecursiveMap(ns, h, [...way], scanned);
        } else if (scanned.get(h)!.length > way.length) {
            scanned.set(h, way);
        }
    }
    return scanned;
}

/** Scan hosts recursively and return list of all available hosts */
export function qScanRecursive(ns: NS): string[] {
    return Array.from(qScanRecursiveMap(ns).keys())
}

export function qExcludedHosts(): Set<string> {
    return new Set("home darkweb".split(" "));
}

export function qScanExceptExcluded(ns: NS, host?: string): string[] {
    return (host ? ns.scan(host) : ns.scan()).filter(x => !qExcludedHosts().has(x));
}

export function qKillThisScriptExceptItself(ns: NS) {
    for (var p of ns.ps()) {
        if (p.filename == ns.getScriptName() && p.pid != ns.pid) {
            ns.tprint(`qKillThisScriptExceptItself: killing ${p.pid} ${p.filename} ${p.args}`);
            ns.kill(p.pid);
        }
    }
}

export namespace dist {
    export function files(): string[] {
        return "lib/my.ts s/d/01.js s/d/02.js s/d/03.js".split(' ')
    }
    export function hash(ns: NS): string {
        var sum = 0;
        for (var f of files()) {
            sum += qStrhash(ns.read(f));
        }
        return `${files()} ${sum}`;
    }
}

export function qDistScanOnce(ns: NS, script?: string): string[] {
    var ret: string[] = [];
    script = script || ns.getScriptName();
    var m = new qMain(ns);
    var hosts: string[] = m.has("host") ? [m.get("host")!] : qScanExceptExcluded(ns);
    for (var host of hosts) {
        var done = new qStorage.Done(host);
        if (!done.has(script) || m.has("force")) {
            done.set(script, true);
            ret.push(host);
        } else {
            log.debug(ns, `ignoring ${host} of ${script}`);
        }
    }
    return ret;
}

export function qGlob(ns: NS, host: string, rgx: RegExp | string): string[] {
    var rgx2: RegExp = typeof (rgx) == "string" ? new RegExp(rgx) : rgx;
    return ns.ls(host).filter(f => rgx2.test(f));
}

export function is_running(ns: NS, host: string, pid: number) {
    return ns.ps(host).find(p => p.pid == pid);
}

export async function wait(ns: NS, host: string, pid: number) {
    while (is_running(ns, host, pid)) {
        await ns.sleep(500);
        ns.tprint(`waiting for pid ${pid} on ${host}`);
    }
}

/**
* @description Get log invoker name
* @return {string} The invoker name
*/
export function getCaller(up: number = 0): string {
    try {
        throw new Error();
    } catch (e: any) {
        try {
            return e.stack.split('at ')[3 + up].split(' ')[0];
        } catch (e) {
            return '';
        }
    }
}

export namespace qStorage {
    export function has(key: string): boolean {
        return key in localStorage;
    }
    export function keys(): string[] {
        var ret: string[] = [];
        for (var i = 0, len = localStorage.length; i < len; ++i) {
            ret.push(localStorage.key(i)!);
        }
        return ret;
    }
    export function get(key: string, def?: any): any {
        var item = localStorage.getItem(key);
        if (!item || item === "undefined") {
            if (def) {
                return def;
            } else {
                throw `no item ${key} in localstorage`;
            }
        }
        return JSON.parse(item);
    }
    export function set(key: string, obj: any) {
        localStorage.setItem(key, JSON.stringify(obj));
    }
    export function remove(key: string) {
        localStorage.removeItem(key);
    }
    export function rmRegex(rgx: string): number {
        var rgx2 = new RegExp(rgx);
        var count = 0;
        for (var key of keys()) {
            if (rgx2.test(key)) {
                localStorage.removeItem(key);
                count++;
            }
        }
        return count;
    }
    export function rmPrefix(prefix: string): number {
        var count = 0;
        for (var key of keys()) {
            if (key.startsWith(prefix)) {
                localStorage.removeItem(key);
                count++;
            }
        }
        return count;
    }
    export function list(dir?: string): string[] {
        return Array.from(keys().filter(p => !dir || p.startsWith(dir))).sort()
    }
    export class Group {
        constructor(public prefix: string) { }
        _key(key: string): string { return `${this.prefix}/${key}`; }
        get(key: string, def?: any): any {
            return qStorage.get(this._key(key), def);
        }
        set(key: string, obj: any) {
            qStorage.set(this._key(key), obj);
        }
        has(key: string): boolean {
            return qStorage.has(this._key(key));
        }
        clear() {
            return rmPrefix(this.prefix);
        }
    }
    export class Host extends Group {
        constructor(public host: string) { super(`host/${host}`); }
    }
    export class Done extends Group {
        constructor(public host: string) { super(`done/${host}`); }
    }
    export function getServer_(host: string): Server {
        return new Host(host).get("getServer");
    }
    export function scan_(host: string): string[] {
        return new Host(host).get("scan");
    }
    export function getServerMoneyAvailable_(host: string): number {
        return new Host(host).get("getServerMoneyAvailable");
    }
    export function hasRootAccess_(host: string): boolean {
        return new Host(host).get("hasRootAccess");
    }
    export function getServerSecurityLevel_(host: string): number {
        return new Host(host).get("getServerSecurityLevel");
    }
}

export namespace log {
    export function _log(ns: NS, ...txt: string[]) {
        var host = ns.getHostname();
        ns.tprint(`${qMain._time()}: ${host}: `, ...txt);
    }
    export function debug(ns: NS, ...txt: string[]) {
        if (0) {
            _log(ns, ...txt);
        }
    }
    export function log(ns: NS, ...txt: string[]) {
        _log(ns, ...txt);
    }
    export function warn(ns: NS, ...txt: string[]) {
        _log(ns, `WARNING: `, ...txt);
    }
    export function error(ns: NS, ...txt: string[]) {
        _log(ns, `ERROR: ERROR ERRRO ERRORER `, ...txt);
    }
}

export function qColumnize(txt: any[][]): string[] {
    var separator = " ";
    var filler = " ";
    var widths: number[] = Array<number>(txt[0].length).fill(0);
    for (var row of txt) {
        for (var i in row) {
            widths[i] = Math.max(widths[i], row[i].toString().length);
        }
    }
    var ret: string[] = [];
    for (var row of txt) {
        var line = "";
        for (var i in row) {
            line = line +
                (line == "" ? "" : separator) +
                row[i].toString().padEnd(widths[i], filler);
        }
        ret.push(line);
    }
    return ret;
}
