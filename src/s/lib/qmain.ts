import {NS} from "@ns";

export const COLORS = {
    black: '\u001b[30m',
    red: '\u001b[31m',
    green: '\u001b[32m',
    yellow: '\u001b[33m',
    blue: '\u001b[34m',
    magenta: '\u001b[35m',
    cyan: '\u001b[36m',
    white: '\u001b[37m',
    brightBlack: '\u001b[30;1m',
    brightRed: '\u001b[31;1m',
    brightGreen: '\u001b[32;1m',
    brightYellow: '\u001b[33;1m',
    brightBlue: '\u001b[34;1m',
    brightMagenta: '\u001b[35;1m',
    brightCyan: '\u001b[36;1m',
    brightWhite: '\u001b[37;1m',
    reset: '\u001b[0m',
};

export enum LogLevel {
    debug,
    info,
    notice,
    warn,
    error,
};

const LOGPREFIX: Map<number, string> = new Map([
    [LogLevel.debug, `${COLORS.cyan}DEBUG:`],
    [LogLevel.info, `${COLORS.white}`],
    [LogLevel.notice, `${COLORS.brightMagenta}WARNING:`],
    [LogLevel.warn, `${COLORS.brightYellow}WARNING:`],
    [LogLevel.error, `${COLORS.brightRed}ERROR:`],
]);

export type qSpec = {
    help?: string;
    min?: number;
    len?: number;
    host?: string;
    opts?: string[];
};

export class qMain {
    args: string[] = [];
    map: Map<string, string> = new Map();
    loglevel: number = LogLevel.info;
    logfile?: string;
    constructor(public ns: NS, public spec?: qSpec) {
        for (var iarg of ns.args) {
            var arg = `${iarg}`;
            if ("--help -h".split(" ").includes(arg)) {
                this.usage();
                ns.exit();
            }
            if (arg.includes("=")) {
                var s = arg.split("=", 2);
                this.map.set(s[0], s[1]);
            } else {
                this.args.push(arg);
            }
        }
        if (spec) {
            if (spec.min) {
                qAssert(this.args.length >= spec.min, `wrong number of arguments`);
            }
            if (spec.len) {
                qAssert(this.args.length == spec.len, `wrong number of arguments`);
            }
        }
        if (this.map.has("debug")) {
            this.loglevel = LogLevel.debug;
        } else if (this.map.has("log")) {
            var log: string = this.map.get("log")!;
            // @ts-ignore: 7015
            this.loglevel = LogLevel[log];
        }
        if (this.map.has("logfile")) {
            if (this.map.get("logfile") == "1") {
                this.logfile = `log/${ns.getScriptName()}.txt`;
            } else if (this.map.get("logfile") == "0") {
            } else {
                this.logfile = `log/${this.map.get("logfile")}.txt`;
            }
        }
    }
    usage() {
        var help: string[] = [];
        help.push(`Usage: ${this.ns.getScriptName()} ?`);
        if (this.spec) {
            if (this.spec.help) {
                for (var line of this.spec.help.split("\n")) {
                    help.push(line);
                }
                help.push(`Arguments: len=${this.spec.len} min=${this.spec.min}`);
            }
        }
        var mainoptions = [
            "log:loglevel to print with, ex. info",
            "log2log:print to tail log",
            "hostname:the host we are executing on, for caching",
            "logfile:the logfile inside log dir to print to",
            "debug:alias to log=debug",
        ];
        help.push("Options in the form var=val:");
        for (var opt of [...(this.spec && this.spec.opts || []), ...mainoptions]) {
            var [o, desc] = opt.split(":");
            help.push(`  ${o}:  ${desc}`);
        }
        for (var line of help) {
            this.ns.tprint(line);
        }
    }
    map2args(): string[] {
        var r: string[] = [];
        for (var [k, v] of this.map.entries()) {
            r.push(`${k}=${v}`);
        }
        return r;
    }
    has(key: string): boolean {
        return this.map.has(key);
    }
    get(key: string, def?: string): string {
        if (!this.map.has(key)) {
            if (def) {
                return def;
            } else {
                this.throw(`no ${key} argument: ${this.ns.getScriptName()} ${this.args}`);
            }
        }
        return this.map.get(key)!;
    }
    static _time(): string {
        var d = new Date();
        return `${d.getHours().toString().padStart(2, "0")}:` +
            `${d.getMinutes().toString().padStart(2, "0")}:` +
            `${d.getSeconds().toString().padStart(2, "0")}`
            //+ `.${d.getMilliseconds().toFixed().padStart(3, '0')}`
            ;
    }
    llog(lvl: LogLevel, ...txt: string[]) {
        if (this.loglevel > lvl) return;
        var host =
            this.spec && this.spec.host ? ` ${this.spec.host}:` :
                this.has("hostname") ? ` ${this.get("hostname")}:` :
                    ``;
        var prefix = LOGPREFIX.get(lvl) || "";
        var str = `${prefix}${qMain._time()}:${host} ` + txt.join(" ") + COLORS.reset;
        if (this.logfile) {
            this.ns.write(this.logfile, `${this.ns.getScriptName()}: ${str}\n`, "a");
            if (lvl >= LogLevel.warn) {
                this.ns.tprint(str);
            }
        } else if (this.map.get("log2log")) {
            this.ns.print(str);
            if (lvl >= LogLevel.warn) {
                this.ns.tprint(str);
            }
        } else {
            this.ns.tprint(str);
        }
    }
    debug(...txt: string[]) {
        this.llog(LogLevel.debug, ...txt);
    }
    log(...txt: string[]) {
        this.llog(LogLevel.info, ...txt);
    }
    info(...txt: string[]) {
        this.llog(LogLevel.info, ...txt);
    }
    notice(...txt: string[]) {
        this.llog(LogLevel.notice, ...txt);
    }
    warn(...txt: string[]) {
        this.llog(LogLevel.warn, ...txt);
    }
    error(...txt: string[]) {
        this.llog(LogLevel.error, ...txt);
    }
    /** Throw assertion and print stacktrace with source files content */
    assert(cond: boolean, ...txt: string[]) {
        if (!cond) {
            this.throw(...txt);
        }
    }
    throw(...txt: string[]): never {
        var err = new Error();
        // at <function> (<file>:<line>:<column>)
        // <filename>:L<line>@<function>
        //var rgx = new RegExp("(.+at.+\\((?<file>[^:]+):(?<line>[0-9]+)|(?<file>[^:]+):L(?<line>[0-9]+)).*");
        //var rgx = new RegExp("(?<file>[^:]+):L(?<line>[0-9]+).*");
        var rgx = new RegExp(".+at (?<func>[^ ]+) \\([^/]+/(?<file>[^:]+):(?<line>[0-9]+).*");
        var arr: string[] = [];
        for (var line of err.stack ? err.stack.split("\n") : []) {
            arr.push(line);
            var res = eval("rgx.exec(line)");
            this.ns.tprint(res, line);
            if (!res || !res.groups) continue;
            var file = res.groups.file;
            if (file == "s/lib/qmain.js" && "qMain.throw qMain.assert".split(" ").includes(res.groups.func)) {
                // Exclude printing yourself.
                continue;
            }
            var linenumber = +res.groups.line - 1;
            var filelines = this.ns.read(file).split("\n");
            if (!(linenumber in filelines)) {
                arr.push(` no line number ${linenumber} in ${file}`);
                continue;
            }
            for (var i = linenumber - 3, max = linenumber + 3; i < max; ++i) {
                if (i in filelines) {
                    var fileline = filelines[i];
                    arr.push(`${file}:${i}: ${fileline}`);
                }
            }
        }
        throw `${txt.join(" ")}\n\n${arr.join('\n')}`;
    }
}

export function qAssert(cond: boolean, ...txt: string[]) {
    if (cond) return;
    var err = new Error();
    throw `${txt.join(" ")}\n\n${err.stack}`;
}
