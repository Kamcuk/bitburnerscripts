import {NS} from "@ns";
import {qColumnize, qMain, qScanRecursive} from "s/lib/my";

export async function main(ns: NS) {
    var m = new qMain(ns);
    var hosts: string[] = m.has("host") ? m.get("host").split(",") : qScanRecursive(ns);
    var legend = "pid host used income gain filename args".split(" ");
    var txt: any[][] = [];
    for (var host of hosts) {
        for (var p of ns.ps(host)) {
            if (!m.has("file") || new RegExp(m.get("file")!).test(p.filename)) {
                var row = [
                    p.pid,
                    host,
                    (ns.getScriptRam(p.filename) * p.threads).toFixed(2),
                    ns.getScriptIncome(p.filename, host, ...p.args).toFixed(0),
                    ns.getScriptExpGain(p.filename, host, ...p.args).toFixed(1),
                    p.filename,
                    p.args.join(' '),
                ];
                txt.push(row);
            }
        }
    }
    txt.sort((a, b) => a[0] - b[0]);
    if (txt.length > 1) {
        for (var line of qColumnize([legend, ...txt])) {
            if (!m.has("grep") || new RegExp(m.get("grep")!).test(line)) {
                ns.tprint(line);
            }
        }
    }
    ns.tprint(`Running ${txt.length - 1} processes`);
}
