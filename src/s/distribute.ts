import {NS} from "@ns";
import {sync} from "s/d/02_sync";
import * as dist from "s/d/10_dist";
import {home, log, qMain, qStorage} from "s/lib/my";
export async function main(ns: NS) {
    var m = new qMain(ns);
    var host: string = m.args[0] || ns.getHostname();
    if (host == home) {
        ns.scriptKill("s/d/12_hack.js", home);
    }
    if (m.args[0] && m.args[1]) {
        var cnt = new qStorage.Done(m.args[0]).clear();
        log.log(ns, `Removed ${cnt} dones with ${m.args[0]}`);
        cnt = new qStorage.Done(m.args[1]).clear();
        log.log(ns, `Removed ${cnt} dones with ${m.args[1]}`);
        if (host != home) {
            log.log(ns, `killall ${m.args[0]}`);
            ns.killall(m.args[0]);
            await sync(ns, host)
        }
    } else {
        var prefix = `done/`;
        var cnt = qStorage.rmPrefix(prefix);
        log.log(ns, `Removed ${cnt} dones with ${prefix}`);
    }
    await dist.dist(ns, host);
}
