import {NS, ProcessInfo} from "@ns";
import {qMain, qScanRecursive} from "s/lib/my";

var m: qMain;

export async function main(ns: NS) {
    m = new qMain(ns);
    var hosts: string[] = m.map.has("host") ? [m.map.get("host")!] : qScanRecursive(ns);
    var processes: [string, ProcessInfo][] = [];
    for (var host of hosts) {
        for (var p of ns.ps(host)) {
            if (!m.map.has("file") || new RegExp(m.map.get("file")!).test(p.filename)) {
                processes.push([host, p]);
            }
        }
    }
    if (processes.length == 0) {
        m.error("no maching proceses");
    } else {
        for (var [host, p] of processes) {
            m.log(`Opening log window for ${p.pid} on ${host}`);
            ns.tail(p.pid, host);
        }
    }
}
