import {NS} from "@ns";
import {home, log, qAssert, qExcludedHosts, qGlob, qMain, qScanRecursive} from "s/lib/my";
export async function main(ns: NS) {
    var m = new qMain(ns);
    var hosts: string[] = m.args.length ? m.args : qScanRecursive(ns).filter(h => !qExcludedHosts().has(h));
    qAssert(!hosts.includes(home), "trying to sync to home!");
    var files = qGlob(ns, home, "^s/");
    m.log(`Copying ${files.length} files to ${hosts}`);
    for (var host of hosts) {
        ns.scp(files, host, home);
    }
}
