import {NS} from "@ns";
import {home, qColumnize, qExcludedHosts, qKillThisScriptExceptItself, qMain, qScanRecursive, qStorage} from "s/lib/my";
import {qGainRoot} from "s/rooter";

var m: qMain;

type DoitOpts = {
    calcthreads?: number | boolean;
    savemem?: number;
};

function doit(ns: NS, host: string, script: string, args: string[], opts?: DoitOpts): number {
    var serverMaxRam = ns.getServerMaxRam(host) - (opts ? opts.savemem || 0 : 0);
    var serverUsedRam = ns.getServerUsedRam(host);
    var scriptRam = ns.getScriptRam(script, host);
    var threads: number = opts && opts.calcthreads ? Math.floor((serverMaxRam - serverUsedRam) / scriptRam) : 1;
    var msg = `exec host=${host} threads=${threads}=(${serverMaxRam}-${serverUsedRam})/${scriptRam}: ${script} ${args}`;
    if (scriptRam == 0) {
        m.error(`could not ${msg}: script missing`);
        return 0;
    }
    if (threads < 1) {
        m.error(`could not ${msg} out of memory`);
        return 0;
    }
    m.assert(threads * scriptRam < serverMaxRam - serverUsedRam);
    m.debug(`${msg}`);
    var pid = ns.exec(script, host, {threads: threads}, ...args, `hostname=${host}`, ...m.map2args());
    if (!pid) {
        m.error(`could not ${msg}`);
        return 0;
    }
    return pid;
}

async function doitwait(ns: NS, host: string, script: string, args: string[], opts?: DoitOpts) {
    var pid = doit(ns, host, script, args, opts);
    if (!pid) return;
    while (ns.ps(host).find(p => p.pid == pid)) {
        await ns.sleep(500);
        m.debug(`waiting for host=${host} pid=${pid} ${script} ${args}`)
    }
}

const ACTIONRGX = new RegExp("^s/(weaken|grow|hack).js$");

function killallActions(ns: NS, hosts: string[]) {
    for (var host of hosts) {
        for (var p of ns.ps(host)) {
            if (ACTIONRGX.test(p.filename)) {
                m.debug(`killing action pid=${p.pid} on ${host}: ${p.filename} ${p.args}`);
                ns.kill(p.pid);
            }
        }
    }
}

type Action = {host: string, action: string, hack: string, pid?: number};
type Actions = {[host: string]: Action}

function action_str(a: Actions, idx: string): string {
    return idx in a ?
        `${idx}=Action(${a[idx].host},${a[idx].action},${a[idx].hack})` :
        `${idx}=Action(,,)`;
}

function actions_diff(a: Actions, b: Actions): Set<string> {
    var ret: Set<string> = new Set();
    for (var k in a) {
        if (!(k in b && a[k].action == b[k].action && a[k].hack == b[k].hack)) {
            ret.add(k);
        }
    }
    for (var k in b) {
        if (!(k in a)) {
            ret.add(k);
        }
    }
    return ret;
}

function chooseAction(ns: NS, host: string): string {
    var securityLevel = ns.getServerSecurityLevel(host);
    var minSecurityLevel = ns.getServerMinSecurityLevel(host);
    var serverMoneyAvailable = ns.getServerMoneyAvailable(host);
    var serverMaxMoney = ns.getServerMaxMoney(host);
    var playerHackingLevel = ns.getHackingLevel();
    var serverRequiredHacking = ns.getServer(host).requiredHackingSkill || 0;
    var msg = `securityLevel=${securityLevel} minSecurityLevel=${minSecurityLevel} serverMoneyAvailable=${serverMoneyAvailable} serverMaxMoney=${serverMaxMoney} playerHackingLevel=${playerHackingLevel} serverRequiredHacking=${serverRequiredHacking}`;
    var action =
        playerHackingLevel < serverRequiredHacking ?
            "" :
            ns.getServerSecurityLevel(host) > 1 && securityLevel > minSecurityLevel + 5 ?
                "weaken" :
                serverMoneyAvailable < serverMaxMoney * 0.75 || serverMoneyAvailable < 1 ?
                    "grow" :
                    "hack";
    //m.debug(`${action} on ${host} because ${msg}`);
    return action;
}

function chooseActions(ns: NS, hosts: string[]): Actions {
    var hosts = hosts.
        filter(h => ns.getServerMaxRam(h) > 0 && ns.hasRootAccess(h)).
        sort((a, b) => ns.getServerMaxRam(b) - ns.getServerMaxRam(a));
    var moneyhosts = hosts.
        filter(h => h != home && chooseAction(ns, h) != "" && ns.getServerMaxMoney(h) > 0).
        sort((a, b) => ns.getServerMoneyAvailable(b) - ns.getServerMoneyAvailable(a));
    m.assert(moneyhosts.length > 0, "moneyhosts.length > 0");
    var ret: Actions = {};
    for (var [i, host] of hosts.entries()) {
        var hack = moneyhosts[i % moneyhosts.length];
        ret[host] = {host: host, action: chooseAction(ns, hack), hack: hack};
    }
    return ret;
}

function periodicActions(): Actions {
    return {};
}

async function runner_main(ns: NS) {
    qKillThisScriptExceptItself(ns);
    var hosts = qScanRecursive(ns);
    qGainRoot(m, hosts);
    hosts = hosts.filter(h => ns.hasRootAccess(h) && !qExcludedHosts().has(h));
    m.debug(`${hosts}`);
    await doitwait(ns, home, "s/sync.js", hosts);
    var hosts = [...hosts, ns.getHostname()]
    while (1) {
        var actions: Actions = {
            ...chooseActions(ns, hosts),
            ...periodicActions(),
        };
        var lastActions: Actions = !qStorage.has("actions") ? [] : qStorage.get("actions");
        var diff: Set<string> = actions_diff(actions, lastActions);
        if (diff.size) {
            for (var idx of diff) {
                m.log(`${action_str(lastActions, idx)} -> ${action_str(actions, idx)}`);
                if (idx in actions) {
                    var a: Action = actions[idx];
                    if (a.pid) {
                        ns.kill(a.pid);
                    }
                    var actionscript = `s/${actions[a.host].action}.js`;
                    a.pid = doit(ns, a.host, actionscript, [actions[a.host].hack], {
                        calcthreads: 1,
                        savemem: idx == home ? 40 : 0,
                    });
                }
            }
            m.log(`Updated ${diff.size} actions running on ${Object.keys(actions).length} hosts hacking ${hosts.length} hosts`);
        }
        qStorage.set("actions", actions);
        await ns.sleep(500);
    }
}

export async function main(ns: NS) {
    ns.disableLog("ALL");
    m = new qMain(ns);
    if (m.args.length == 0) {
        await runner_main(ns);
    } else if (m.args[0] == "rm") {
        var lastActions: Actions = !qStorage.has("actions") ? [] : qStorage.get("actions");
        delete lastActions[m.args[1]];
        qStorage.set("actions", lastActions);
        m.notice(`element ${m.args[1]} removed`);
    } else if (m.args[0] == "ps") {
        var legend = "host th file hack money grow".split(" ");
        var txt: any[][] = [];
        for (var host of qScanRecursive(ns)) {
            for (var p of ns.ps(host)) {
                if (ACTIONRGX.test(p.filename)) {
                    var hack = p.args.find(a => !`${a}`.includes("="))!.toString();
                    txt.push([
                        host, p.threads, p.filename, hack,
                        ns.getServerMoneyAvailable(hack).toFixed(0) + "/" + ns.getServerMaxMoney(hack).toFixed(0),
                        ns.getGrowTime(hack).toFixed(0)]);
                }
            }
        }
        txt.sort((a, b) => a.at(-1) == b.at(-1) ? a[0] > b[0] ? -1 : 1 : a.at(-1) > b.at(-1) ? -1 : 1);
        for (var line of qColumnize([legend, ...txt])) {
            m.log(line);
        }
    } else {
        m.assert(false, "invalid arguments");
    }
}
