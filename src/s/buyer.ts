import {NS} from "@ns";
import {qMain} from "s/lib/qmain";
export async function main(ns: NS) {
    var m = new qMain(ns);
    switch (m.args[0]) {
        case "ls": {
            ns.tprint("limit=", ns.getPurchasedServerLimit());
            ns.tprint("maxram=", ns.getPurchasedServerMaxRam());
            ns.tprint("cat=", ns.getPurchasedServerUpgradeCost("1", 1 * 1024 * 1024));
        }
    }
}
