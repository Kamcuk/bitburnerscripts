import {NS} from "@ns";
export async function main(ns: NS) {
    var out: string = "";
    for (var arg of ns.args) {
        out += (out == "" ? "" : " ") + `${arg}`;
    }
    ns.tprint(out);
}
