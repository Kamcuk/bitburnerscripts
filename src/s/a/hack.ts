import {NS} from "@ns";
export async function main(ns: NS) {
    var host = ns.args[0].toString();
    ns.print(`hack ${host}`);
    await ns.hack(host);
}
