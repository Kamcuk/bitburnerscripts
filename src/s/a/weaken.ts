import {NS} from "@ns";
export async function main(ns: NS) {
    var host = ns.args[0].toString();
    ns.print(`weaken ${host}`);
    await ns.weaken(host);
}
