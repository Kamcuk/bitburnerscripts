import {NS} from "@ns";
import {qKillThisScriptExceptItself, qMain} from "s/lib/my";

var m: qMain;
var positions: Map<string, number> = new Map();

function cat() {
    var ns = m.ns;
    for (var file of m.args) {
        for (var [linenum, line] of ns.read(file).split("\n").entries()) {
            if (
                line != "" &&
                (!positions.has(file) || (linenum > positions.get(file)!)) &&
                (!m.has("grep") || new RegExp(m.get("grep")!).test(line)) &&
                (!m.has("filter") || !new RegExp(m.get("filter")!).test(line))
            ) {
                positions.set(file, linenum);
                var pre = m.has("tail") && m.args.length > 1 ? `${file}: ` : "";
                var n = m.has("n") ? `${linenum}: ` : "";
                ns.tprint(`${pre}${n}${line}`);
            }
        }
    }
}

async function tail() {
    var ns = m.ns;
    while (1) {
        cat();
        await ns.sleep(500);
    }
}

export async function main(ns: NS) {
    m = new qMain(ns, {
        min: 1,
        opts: [
            "n:show line numbers",
            "grep:filter lines",
            "tail:work in tail mode",
            "filter:filter lines",
        ]
    });
    qKillThisScriptExceptItself(ns);
    if (m.has("tail")) {
        await tail();
    } else {
        cat();
    }
}
