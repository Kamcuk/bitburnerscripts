import {NS} from "@ns";
import {qMain, qScanRecursive} from "s/lib/my";

class Opener {
    constructor(public open: boolean, public file: string, public act: (host: string) => void) {}
};

function gainRoot(m: qMain, host: string) {
    var ns = m.ns;
    if (ns.hasRootAccess(host)) return;
    var neededPorts = ns.getServerNumPortsRequired(host);
    var s = ns.getServer(host);
    var openers: Opener[] = [
        new Opener(s.sshPortOpen, "BruteSSH", ns.brutessh),
        new Opener(s.ftpPortOpen, "FTPCrack", ns.ftpcrack),
        new Opener(s.smtpPortOpen, "relaySMTP", ns.relaysmtp),
        new Opener(s.httpPortOpen, "HTTPWorm", ns.httpworm),
        new Opener(s.sqlPortOpen, "SQLInject", ns.sqlinject),
    ];
    for (var o of openers) {
        if (neededPorts > 0) {
            if (o.open) {
                --neededPorts;
            } else if (ns.fileExists(`${o.file}.exe`)) {
                m.log(`running ${neededPorts} ${o.file} ${host}`);
                o.act(host);
                --neededPorts;
            }
        }
    }
    if (neededPorts > 0) {
        m.log(`skip ports=${neededPorts} ${host}`);
        return;
    }
    m.assert(ns.fileExists("NUKE.exe"));
    ns.nuke(host);
    m.warn(`gained root access to ${host} with ${ns.hasRootAccess(host)}`);
}

export function qGainRoot(m: qMain, hosts: string[]) {
    for (var host of hosts) {
        gainRoot(m, host);
    }
}


export async function main(ns: NS) {
    var m = new qMain(ns);
    var hosts = qScanRecursive(ns);
    qGainRoot(m, hosts);
}
