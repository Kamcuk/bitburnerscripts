import {NS} from "@ns";
import {home, log, qMain, qStorage} from "s/lib/my";
export async function main(ns: NS) {
    var m = new qMain(ns);
    var hostStorage = new qStorage.Host(ns.getHostname());
    var host = hostStorage.get("hostToHack");
    if (!host || host == home) return;
    while (true) {
        var securityLevel = qStorage.getServerSecurityLevel_(host);
        var minSecurityLevel = ns.getServerMinSecurityLevel(host);
        var serverMoneyAvailable = ns.getServerMoneyAvailable(host);
        var serverMaxMoney = ns.getServerMaxMoney(host);
        //
        hostStorage.set("getServerMinSecurityLevel", minSecurityLevel);
        hostStorage.set("getServerMoneyAvailable", serverMoneyAvailable);
        hostStorage.set("getServerMaxMoney", serverMaxMoney);
        //
        var msg = `securityLevel=${securityLevel} minSecurityLevel=${minSecurityLevel} serverMoneyAvailable=${serverMoneyAvailable} serverMaxMoney=${serverMaxMoney}`;
        if (securityLevel > minSecurityLevel + 5) {
            ns.print(`weaken ${host} because ${msg}`);
            await ns.weaken(host);
        } else if (serverMoneyAvailable < serverMaxMoney * 0.75) {
            ns.print(`grow ${host} because ${msg}`);
            await ns.grow(host);
        } else {
            ns.print(`hack ${host} because ${msg}`);
            if (!await ns.hack(host)) {
                log.error(ns, `hacking was unsuccefull`);
            }
        }
    }
}
