import { NS } from "@ns";
import { log, qMain, qDistScanOnce } from "s/lib/my";
export function doit(ns: NS, host: string, script: string, calcthreads?: number): number {
    var serverMaxRam = ns.getServerMaxRam(host);
    var serverUsedRam = ns.getServerUsedRam(host);
    var scriptRam = ns.getScriptRam(script, host);
    var threads: number = !calcthreads ? 1 : Math.floor((serverMaxRam - serverUsedRam) / scriptRam);
    var msg = `exec host=${host} threads=${threads}=(${serverMaxRam}-${serverUsedRam})/${scriptRam}: script=${script} host=${host}`;
    if (scriptRam == 0) {
        log.error(ns, `could not ${msg}: script missing`);
        return 0;
    }
    if (threads == 0) {
        log.error(ns, `could not ${msg} out of memory`);
        return 0;
    }
    var pid = ns.exec(script, host, threads, `host=${host}`);
    if (!pid) {
        log.error(ns, `could not ${msg}`);
        return 0;
    }
    log.log(ns, `${msg} ${pid}`);
    return pid;
}
export async function doitwait(ns: NS, host: string, script: string, calcthreads?: number) {
    var pid = doit(ns, host, script, calcthreads);
    if (!pid) return;
    while (ns.ps(host).find(p => p.pid == pid)) {
        await ns.sleep(500);
        log.debug(ns, `waiting for script=${script} host=${host} pid=${pid}`)
    }
}
export async function dist(ns: NS, host: string) {
    await doitwait(ns, host, "s/d/01_killall.js");
    await doitwait(ns, host, "s/d/02_sync.js");
    await doitwait(ns, host, "s/d/03_info1.js");
    await doitwait(ns, host, "s/d/04_info2.js");
    await doitwait(ns, host, "s/d/05_info3.js");
    await doitwait(ns, host, "s/d/10_dist.js");
    await doitwait(ns, host, "s/d/11_root.js");
    await doitwait(ns, host, "s/d/12_backdoor.js", 1);
    doit(ns, host, "s/d/13_hack.js", 1);
}
export async function main(ns: NS) {
    var m = new qMain(ns);
    for (var host of qDistScanOnce(ns)) {
        await dist(ns, host);
    }
}
