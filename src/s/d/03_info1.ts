import {NS} from "@ns";
import {qDistScanOnce, qStorage} from "s/lib/my";
export async function main(ns: NS) {
    for (var host of qDistScanOnce(ns)) {
        var hostInfo = new qStorage.Host(host);
        var server = ns.getServer(host);
        hostInfo.set("getServer", server);
        hostInfo.set("backdoorInstalled", server.backdoorInstalled);
    }
}
