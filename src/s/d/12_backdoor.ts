import {NS} from "@ns";
import {log, qMain, qStorage} from "s/lib/my";
export async function main(ns: NS) {
    var m = new qMain(ns);
    var host = m.args[0] || ns.getHostname();
    var srv = qStorage.getServer_(host);
    if (!srv.backdoorInstalled) {
        var reqHack = ns.getServerRequiredHackingLevel(host);
        var playerHack = ns.getPlayer().skills.hacking;
        if (playerHack > reqHack) {
            log.log(ns, `Installing backdoor`);
            ns.singularity.installBackdoor();
        }
    }
}
