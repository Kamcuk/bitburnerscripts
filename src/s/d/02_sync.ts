import {NS} from "@ns";
import {home, log, qMain, qDistScanOnce, qGlob} from "s/lib/my";

export async function sync(ns: NS, host: string) {
    if (host == home) {
        throw `Trying to sync home`;
    }
    var rgx = "^s/(d|lib)/";
    var remote = qGlob(ns, host, rgx);
    if (remote.length) {
        log.log(ns, `Removing ${remote} from ${host}`)
        remote.forEach(f => ns.rm(f, host));
    }
    var files = qGlob(ns, home, rgx);
    log.log(ns, `Copying ${files} to ${host}`)
    ns.scp(files, host, home);
}

export async function main(ns: NS) {
    var m = new qMain(ns);
    if (m.args[0]) {
        await sync(ns, m.args[0]);
    } else {
        for (var host of qDistScanOnce(ns)) {
            await sync(ns, host);
        }
    }
}
