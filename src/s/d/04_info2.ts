import { NS } from "@ns";
import { qDistScanOnce, qStorage } from "s/lib/my";
export async function main(ns: NS) {
    for (var host of qDistScanOnce(ns)) {
        var t = new qStorage.Host(host);
        t.set("scan", ns.scan(host));
        t.set("getServerMoneyAvailable", ns.getServerMoneyAvailable(host));
        t.set("getServerSecurityLevel", ns.getServerSecurityLevel(host));
        t.set("getServerMaxMoney", ns.getServerMaxMoney(host));
    }
}
