import { NS } from "@ns";
import { qDistScanOnce, log } from "s/lib/my";
export async function main(ns: NS) {
    log.debug(ns, `running`)
    for (var host of qDistScanOnce(ns)) {
        if (ns.ps(host).length > 0) {
            log.log(ns, `killall ${host}`)
            ns.killall(host);
        }
    }
    log.debug(ns, `done`)
}
