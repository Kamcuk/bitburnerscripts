import {NS} from "@ns";
import {log, qDistScanOnce, qMain, qStorage} from "s/lib/my";

function gainRoot(ns: NS, host: string) {
    if (ns.hasRootAccess(host)) return;
    var p = ns.getServerNumPortsRequired(host);
    if (p-- > 0) {
        log.log(ns, `running ${p} brutessh ${host}`);
        ns.brutessh(host);
    }
    if (p-- > 0) {
        log.log(ns, `running ${p} relaysmtp ${host}`);
        ns.relaysmtp(host);
    }
    if (p-- > 0) {
        log.log(ns, `running ${p} ftpcrack ${host}`);
        ns.ftpcrack(host);
    }
    if (p-- > 0) {
        log.log(ns, `running ${p} httpworm ${host}`);
        ns.httpworm(host);
    }
    if (p > 0) {
        log.log(ns, `skip ports=${p} ${host}`);
        return;
    }
    log.log(ns, `nuking ${host}`);
    ns.nuke(host);
    log.log(ns, `fained root access to ${host} with ${ns.hasRootAccess(host)}`);
}

function chooseToHack(ns: NS, hosts: string[]): string | null {
    var host = ns.getHostname();
    var money = ns.getServerMoneyAvailable(host);
    for (var h of hosts) {
        var availmoney = ns.getServerMoneyAvailable(h);
        if (money < availmoney && ns.hasRootAccess(host)) {
            money = availmoney;
            host = h;
        }
    }
    if (!ns.hasRootAccess(host)) {
        log.error(ns, `no root access anywhere`);
        return null;
    }
    return host;
}

export async function main(ns: NS) {
    var m = new qMain(ns);
    var hosts = qDistScanOnce(ns);
    for (var host of hosts) {
        gainRoot(ns, host);
    }
    //
    var hostToHack = chooseToHack(ns, hosts);
    new qStorage.Host(ns.getHostname()).set("hostToHack", hostToHack);
}
