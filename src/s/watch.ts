import {NS} from "@ns";
import {qKillThisScriptExceptItself, qMain, qStrhash} from "s/lib/my";
var fileshash: Map<string, number> = new Map();
var filesmem: Map<string, number> = new Map();

function getfiles(ns: NS): string[] {
    return ns.ls(ns.getHostname()).filter(f => f.endsWith(".js"));
}

function out(ns: NS, file: string, suffix?: string) {
    var mem = ns.getScriptRam(file);
    var inc = mem - (filesmem.get(file) || 0);
    var memstr = mem.toFixed(2).toString().padStart(5);
    var incsign = inc == 0 ? " " : inc < 0 ? "-" : "+";
    var incstr = (incsign + Math.abs(inc).toFixed(2).toString()).padStart(6);
    file = file.padEnd(20);
    ns.tprint(`${file} ${memstr}GB ${incstr}GB ${suffix || ""}`);
    filesmem.set(file, mem);
}

function process_childs(ns: NS, changed: string) {
    var files = getfiles(ns);
    files.filter(f => f == changed);
    var rgx = new RegExp(`from "s/lib/my";$`);
    for (var file of files) {
        var txt = ns.read(file);
        if (rgx.test(txt)) {
            var mem = ns.getScriptRam(file);
            if (filesmem.get(file) != mem) {
                out(ns, file, "because my.js changed");
            }
        }
    }
}

export async function main(ns: NS) {
    var m = new qMain(ns, {len: 0});
    qKillThisScriptExceptItself(ns);
    // initialize filesmem
    for (var file of getfiles(ns)) {
        filesmem.set(file, ns.getScriptRam(file));
    }
    while (true) {
        for (var file of getfiles(ns)) {
            var hash = qStrhash(ns.read(file));
            if (fileshash.get(file) != hash) {
                if (fileshash.has(file) && file == ns.getScriptName()) {
                    m.log(`${ns.getScriptName()} restarting...`);
                    await ns.sleep(1000);
                    ns.run(ns.getScriptName());
                    ns.exit();
                }
                fileshash.set(file, hash);
                out(ns, file);
                process_childs(ns, file);
            }
        }
        await ns.sleep(1000);
    }
}
