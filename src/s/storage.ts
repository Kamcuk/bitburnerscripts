import {NS} from "@ns";
import {log, qAssert, qMain, qStorage} from "s/lib/my";
export async function main(ns: NS) {
    var m = new qMain(ns);
    var cdfile = "tmp/storage.ts.dir.txt";
    var dir = ns.fileExists(cdfile) ? ns.read(cdfile) : "";
    switch (m.args[0]) {
        case "ls": {
            for (var path of qStorage.list(m.args[1])) {
                if (m.has("grep") && !new RegExp(m.get("grep")!).test(path)) continue;
                if (m.has("filter") && m.get("filter")!.split(",").some(f => new RegExp(f).test(path))) continue;
                if (m.has("prefix") && path.startsWith(m.get("prefix")!)) continue;
                ns.tprint(`${path}=${qStorage.get(path)}`);
            }
            break;
        }
        case "set": {
            qAssert(m.args.length == 2, "two arguments needed");
            qStorage.set(m.args[1], m.args[2]);
            ns.tprint(`set ${m.args[1]}=${qStorage.get(m.args[1])}`);
            break;
        }
        case "get": {
            qAssert(m.args.length == 1, "one argument needed");
            ns.tprint(`${qStorage.get(m.args[1])}`);
            break;
        }
        case "rm": {
            qAssert(m.args.length == 2, "one argument needed");
            qAssert(qStorage.has(m.args[1]), `no key ${m.args[1]}`);
            qStorage.remove(m.args[1]);
            m.log(`${m.args[1]} removed`);
            break;
        }
        case "cd": {
            qAssert(m.args.length == 1, "one argument needed");
            if (m.args[1] == "..") {
                if (dir != "") {
                    var dirs = dir.split("/");
                    dirs.pop();
                    dir = dirs.join("/");
                }
            }
            log.log(ns, `cd ${dir}`);
            ns.write(cdfile, `${dir}/${m.args[1]}`);
            break;
        }
        default: {
            ns.tprint(`
Usage: set a b
       get a
       ls [dir]
${m}`);
        }
    }
}
